var express = require('express');
var _ = require('underscore');
var expect = require('chai').expect;
var generator = require('../src/generator');

describe('Message generation tests', function() {
  var host = 'localhost';
  var port = 8080;
  var messages = [];

  before(function() {
    messages = [];
  });

  it('should messages be generated on configured host until program ends', function(done) {
    this.timeout(3000);

    // given a started server
    var app = express();
    app.use(express.urlencoded())
      .use(express.json())
      .use(express.methodOverride())
      .use(app.router)
      .put('/stats/:game', function(req, res) {
        messages.push({game: req.params.game, operation: 'add stat', value: req.body});
        res.send(204);
      });
    app.listen(port, host, function(err) {
      expect(err).not.to.exist;
      // when starting generator for two seconds
      generator.start(host, port, false, function() {
        done('unexpected generator end');
      });
      _.delay(function() {
        // then multiple messages were issued
        expect(messages.length).to.be.greaterThan(0);
        done();
      }, 2000);
    });
    
  });

});