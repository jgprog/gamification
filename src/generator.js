var yaml = require('js-yaml');
var request = require('request');
var _ = require('underscore');
var fs = require('fs');
var join = require('path').join;
var verbose = process.env.NODE_ENV !== 'test';

var games = {};
var gameNames = [];

// return a random integer, within [min] (inclusive, default to 0) and [max] (exclusive)
var randomInt = function(max, min) {
  min = min ? min : 0;
  return Math.floor(Math.random() * (max - min) + min);
};

// loads different games data
var loadGameData = function(done) {
  games = {};
  gameNames = [];
  var folder = join(__dirname, '..', 'data');
  fs.readdir(folder, function(err, entries) {
    if (err) {
      console.error('-- failed to read data folder', err.message);
      return done(err);
    }
    _.each(entries, function(entry) {
      var key = entry.replace('.yaml', '');
      games[key] = yaml.safeLoad(fs.readFileSync(join(folder, entry), 'utf8'));
      gameNames.push(key);
    });
    done();
  });
};

var generateAndSend = function(uri, done) {
  var game = gameNames[randomInt(gameNames.length)];
  var users = games[game].users;
  var badges = games[game].badges;
  var message = {
    player: users[randomInt(users.length)],
    badge: badges[randomInt(badges.length)]
  };
  verbose && console.log('> send badge "' + message.badge + '" awarded to "' + message.player + '" on ', game);
  // effectively send the request
  request.put({
      uri: uri + game,
      json: true,
      body: message
    }, done
  );
};


module.exports = {

  start: function(host, port, bail, end) {
    loadGameData(function(err) {
      if (err) {
        return end(err);
      }
      // stat addition endpoint
      var uri = 'http://' + host + ':' + port + '/blabla/';
      // number of seconds before next request
      var sleep = 0;

      // request end callback: log optionnal [err], bail if necessary, and if not, 
      // issue another request within 3 seconds.
      var requestEnd =  function(err) {
        if (err) {
          verbose && console.log('< failed to send request to "' + host + ':' + port +'":', err.message);
          // exit on first error if bail specified
          if (bail) {
            return end();
          }
        } else {
          console.log('< success');
        }
        // and throw within 3 seconds.
        sleep = Math.floor(Math.random()*4);
        _.delay(generateAndSend, sleep*1000, uri, requestEnd);
      };

      // first request
      generateAndSend(uri, requestEnd);
    });
  }
};