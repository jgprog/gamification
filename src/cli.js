var generator = require('./generator');
var program = require('commander');
var descriptor = require('../package.json');

module.exports = function(args) {
  program
    .version(descriptor.version)
    .option('-h, --host <string>', 'Host server IP, default to "localhost"', 'localhost')
    .option('-p, --port <integer>', 'Host port, default to 80', 8080, parseInt)
    .option('-b, --bail [boolean]', 'Stop at first error, default to false', function(val) {
      return val && val.toLowerCase() === 'true';
    })
    .parse(args);

  console.log('-- starting game 1...\n(hit CTRL+C to stop)');
  generator.start(program.host, program.port, program.bail, function(err) {
    if (err) {
      console.error('-- unexpected generator end:', err.message);
    }
    process.exit(err ? 1 : 0);
  });
};